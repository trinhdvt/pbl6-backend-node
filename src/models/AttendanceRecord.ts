import {BelongsTo, Column, ForeignKey, Model, Table} from "sequelize-typescript";
import Examination from "./Examination";

@Table({
    tableName: "pbl6_attendance_record"
})
class AttendanceRecord extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @ForeignKey(() => Examination)
    @Column({
        field: "exam_id",
    })
    examId!: number;

    @BelongsTo(() => Examination, 'exam_id')
    exam: Examination;

    @Column
    status!: Boolean;
    @Column
    checkAt!: Date;
    @Column
    studentName!: string;
    @Column
    className!: string;
    @Column
    studentId!: string;
    @Column
    facultyName!: string;
    @Column
    birthday!: string;
    @Column
    year!: string;
    @Column
    faceImg!: string;
    @Column
    cardImg!: string;
    @Column
    cropCard!: string;
    @Column
    ipAddress!: string;
    @Column
    userAgent!: string;

}

export default AttendanceRecord;