import {Sequelize} from "sequelize-typescript";
import User from "./User";
import Examination from "./Examination";
import AttendanceRecord from "./AttendanceRecord";

const sequelize = new Sequelize({
    database: "do-an-lang-thang",
    username: "root",
    password: "illusion",
    host: "langthang.cmk1otxtgp2r.ap-southeast-1.rds.amazonaws.com",
    dialect: "mysql",
    pool: {
        max: 20,
        min: 5,
        acquire: 3000,
        idle: 10000
    },
    benchmark: true,
    define: {
        underscored: true,
        timestamps: false
    },
    logging: false
});

sequelize.addModels(
    [User, Examination, AttendanceRecord]
);

export default sequelize;