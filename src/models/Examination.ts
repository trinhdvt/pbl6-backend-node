import {BelongsTo, Column, ForeignKey, HasMany, Model, Table} from "sequelize-typescript";
import AttendanceRecord from "./AttendanceRecord";
import User from "./User";

@Table({
    tableName: "pbl6_examination"
})
class Examination extends Model {

    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @HasMany(() => AttendanceRecord, "exam_id")
    record?: AttendanceRecord[];

    @ForeignKey(() => User)
    @Column({
        field: "admin_id",
    })
    adminId!: number;

    @BelongsTo(() => User, 'admin_id')
    creator: User;

    @Column
    examCode!: string;

    @Column
    detail!: string;

    @Column
    openAt!: Date;

    @Column
    closeAt!: Date;
}

export default Examination;