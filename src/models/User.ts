import {Column, DataType, HasMany, Model, Table} from "sequelize-typescript";
import Role from "./Role";
import Examination from "./Examination";

@Table({
    tableName: "pbl6_user",
})
class User extends Model {
    @Column({
        primaryKey: true,
        autoIncrement: true,
    })
    id!: number;

    @HasMany(() => Examination, "admin_id")
    createdExam?: Examination[];

    @Column
    email!: string;

    @Column
    password!: string;

    @Column
    fullName!: string;

    @Column
    createdAt!: Date;

    @Column
    refreshToken!: string;

    @Column
    accessToken!: string;

    @Column({
        type: DataType.ENUM({values: Object.keys(Role)}),
        defaultValue: Role.TEACHER
    })
    role!: Role;
}

export default User;