import {Body, CookieParam, ForbiddenError, HeaderParam, JsonController, Post, Res} from "routing-controllers";
import {Response} from "express";
import LoginPayload from "../dto/request/LoginPayload";
import {Service} from "typedi";
import AuthService from "../service/AuthService";
import {StatusCodes} from "http-status-codes";
import ms from "ms";

@JsonController("/auth")
@Service()
export default class AuthController {


    constructor(private authService: AuthService) {
    }

    @Post("/login")
    async login(@Body() {email, password}: LoginPayload,
                @Res() res: Response) {

        const {accessToken, refreshToken} = await this.authService.login(email, password);

        return res.status(StatusCodes.OK)
            .cookie("refresh-token", refreshToken, {
                httpOnly: true,
                maxAge: ms('0.5 y')
            }).json({token: accessToken});
    }

    @Post("/refresh")
    async refreshAccessToken(@HeaderParam("Authorization") accessToken: string,
                             @CookieParam("refresh-token") refreshToken: string,
                             @Res() res: Response) {

        if (!accessToken.startsWith("Bearer ")) {
            throw new ForbiddenError("Invalid Authorization header");
        }

        const newToken = await this.authService.reCreateToken(accessToken.substring(7), refreshToken);

        return res.status(StatusCodes.OK)
            .cookie("refresh-token", newToken.refreshToken, {
                httpOnly: true,
                maxAge: ms('0.5 y')
            }).json({token: newToken.accessToken});

    }


}