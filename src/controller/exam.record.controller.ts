import {Authorized, Body, Get, HttpCode, JsonController, Param, Post, Put} from "routing-controllers";
import {Service} from "typedi";
import ExamRecordService from "../service/ExamRecordService";
import RecordPayload from "../dto/request/RecordPayload";
import AttendancePayload from "../dto/request/AttendancePayload";
import Role from "../models/Role";

@JsonController()
@Service()
export default class ExamRecordController {

    constructor(private examRecordService: ExamRecordService) {
    }

    @Get("/exams/:examId/records")
    @Authorized([Role.TEACHER])
    @HttpCode(200)
    public async getAttendance(@Param("examId") examId: number) {
        return await this.examRecordService.getAttendance(examId);
    }

    @Put("/records/:recordId")
    @Authorized([Role.TEACHER])
    @HttpCode(200)
    public async modifyAttendance(@Param("recordId") recordId: number,
                                  @Body() recordPayload: RecordPayload) {

        return await this.examRecordService.modifyAttendance(recordId, recordPayload);
    }

    @Post("/records")
    @HttpCode(200)
    public async submitRecord(@Body() payload: AttendancePayload) {
        return await this.examRecordService.submitRecord(payload);
    }

}
