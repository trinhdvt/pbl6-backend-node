import {
    Authorized,
    BadRequestError,
    Body,
    CurrentUser,
    Get,
    JsonController,
    Param,
    Post,
    Put,
    QueryParam,
    Res
} from "routing-controllers";
import {Service} from "typedi";
import ExamService from "../service/ExamService";
import {Response} from "express";
import ExamPayload from "../dto/request/ExamPayload";
import Role from "../models/Role";
import {StatusCodes} from "http-status-codes";

@JsonController()
@Service()
export default class ExamController {

    constructor(private examService: ExamService) {
    }

    @Get("/exams")
    @Authorized([Role.TEACHER])
    public async getAllExams(@Res() res: Response) {
        let examList = await this.examService.getAllExams();

        return res.status(200).json(examList);
    }

    @Post("/exams")
    @Authorized([Role.TEACHER])
    public async createExam(@Body() exam: ExamPayload,
                            @Res() res: Response,
                            @CurrentUser() user) {

        if (exam.openAt.getTime() < Date.now()) {
            throw new BadRequestError("Thời gian bắt đầu phải sau thời điểm hiện tại!");
        }
        if (exam.closeAt.getTime() <= exam.openAt.getTime()) {
            throw new BadRequestError("Thời gian kết thúc phải sau thời gian bắt đầu!");
        }

        const userId = user.id;
        const examDTO = await this.examService.createExam(exam, userId);
        return res.status(200).json(examDTO);
    }

    @Put("/exams/:examId")
    @Authorized([Role.TEACHER])
    public async editExam(@Body() examPayload: ExamPayload,
                          @Param("examId") examId: number,
                          @CurrentUser() user,
                          @Res() res: Response) {

       if (examPayload.closeAt.getTime() <= examPayload.openAt.getTime()) {
            throw new BadRequestError("Thời gian kết thúc phải sau thời gian bắt đầu!")
        }

        const examDTO = await this.examService.editExam(examPayload, examId);
        return res.status(200).json(examDTO);
    }

    @Get("/exams/check")
    public async timeCheck(
        @QueryParam("examCode", {required: true}) examCode: string,
        @Res() resp: Response) {

        await this.examService.isAbleToTakeExam(examCode);
        return resp.sendStatus(StatusCodes.NO_CONTENT);
    }
}
