import {Container} from "typedi";
import JwtService from "../service/JwtService";
import {Action} from "routing-controllers";

const jwtService = Container.get(JwtService);

export async function PreAuthorize(action: Action, roles: string[]): Promise<boolean> {
    const authHeader = action.request.headers['authorization'];
    if (!authHeader) {
        return false;
    }

    const token = authHeader.replace("Bearer ", "");
    try {
        if (jwtService.isValidJwt(token)) {
            if (roles.length == 0) {
                return true;
            }

            const {role} = jwtService.getPayLoad(token);
            return roles.indexOf(role) != -1;
        }
    } catch (e) {
    }
    return false;
}

export async function CurrentUserChecker(action: Action) {
    if (await PreAuthorize(action, [])) {
        const token = action.request.headers['authorization'].replace("Bearer ", "");

        return jwtService.getPayLoad(token);
    }
}