import Examination from "../../models/Examination";

export default class ExamDTO {

    id: number;
    teacherName: string;
    examCode: string;
    detail: string;
    openAt: Date;
    closeAt: Date;

    constructor(id: number, teacherName: string, examCode: string, detail: string, openAt: Date, closeAt: Date) {
        this.id = id;
        this.teacherName = teacherName;
        this.examCode = examCode;
        this.detail = detail;
        this.openAt = openAt;
        this.closeAt = closeAt;
    }

    public static toDTO(exam: Examination): ExamDTO {
        return new ExamDTO(
            exam.id,
            exam.creator?.fullName,
            exam.examCode,
            exam.detail,
            exam.openAt,
            exam.closeAt
        );
    }
}