import AttendanceRecord from "../../models/AttendanceRecord";

export default class RecordDTO {
    id: number;
    status: Boolean;
    checkAt: Date;
    studentId: string;
    studentName: string;
    className: string;
    facultyName: string;
    birthday: string;
    year: string;
    ipAddress: string;
    faceImg: string;
    cardImg: string;
    cropCard: string;
    userAgent: string;

    public static toDTO(record: AttendanceRecord): RecordDTO {
        const dto = new RecordDTO();
        dto.id = record.id;
        dto.status = record.status;
        dto.checkAt = record.checkAt;
        dto.studentId = record.studentId;
        dto.studentName = record.studentName;
        dto.className = record.className;
        dto.facultyName = record.facultyName;
        dto.birthday = record.birthday;
        dto.year = record.year;
        dto.ipAddress = record.ipAddress;
        dto.faceImg = record.faceImg;
        dto.cardImg = record.cardImg;
        dto.cropCard = record.cropCard;
        dto.userAgent = record.userAgent;
        return dto;
    }
}