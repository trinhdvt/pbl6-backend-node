import {IsEmail, IsNotEmpty} from "class-validator";

export default class LoginPayload {

    @IsNotEmpty()
    @IsEmail()
    public email!: string;

    @IsNotEmpty()
    public password!: string;
}
