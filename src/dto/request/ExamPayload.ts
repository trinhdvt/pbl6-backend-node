import {IsDate, IsNotEmpty} from "class-validator";
import {Type} from "class-transformer";

export default class ExamPayload {
    @IsNotEmpty({message: "Không được để trống tên ca thi"})
    examCode!: string;

    @IsNotEmpty({message: "Không được để trông thông tin ca thi"})
    details!: string;

    @IsNotEmpty({message: "Không được để trống thời gian bắt đầu"})
    @IsDate()
    @Type(() => Date)
    openAt!: Date;

    @IsNotEmpty({message: "Không được để trống thời gian kết thúc"})
    @IsDate()
    @Type(() => Date)
    closeAt!: Date;
}