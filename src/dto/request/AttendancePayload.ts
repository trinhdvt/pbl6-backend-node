import {IsDate, IsNotEmpty, IsOptional} from "class-validator";
import RecordPayload from "./RecordPayload";
import {Type} from "class-transformer";

export default class AttendancePayload extends RecordPayload {
    @IsNotEmpty()
    examCode!: string;
    ipAddress!: string;
    faceImg!: string;
    cardImg!: string;
    cropCard!: string;
    userAgent!: string;

    @IsOptional()
    @IsDate()
    @Type(() => Date)
    checkAt: Date;
}