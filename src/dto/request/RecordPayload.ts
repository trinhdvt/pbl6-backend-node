import {Transform} from "class-transformer";

export function ToBoolean() {
    return Transform(v => ["1", 1, "true", true].includes(String(v).toLowerCase()));
}

export default class RecordPayload {
    @ToBoolean()
    status!: Boolean;
    studentId!: string;
    studentName!: string;
    className!: string;
    facultyName!: string;
    birthday!: string;
    year!: string;
}