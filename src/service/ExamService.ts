import {Service} from "typedi";
import ExamPayload from "../dto/request/ExamPayload";
import Examination from "../models/Examination";
import ExamDTO from "../dto/response/ExamDTO";
import User from "../models/User";
import {BadRequestError, HttpError, NotFoundError} from "routing-controllers";
import {StatusCodes} from "http-status-codes";

@Service()
export default class ExamService {

    async getAllExams() {
        let examinations = await Examination.findAll({
            include: [{
                model: User,
                as: 'creator'
            }]
        });

        return examinations.map(exam => ExamDTO.toDTO(exam));

    }

    async createExam(examPayload: ExamPayload, userId: any) {
        let exam = await Examination.findOne({
            where: {
                examCode: examPayload.examCode
            }
        });
        if (exam) {
            throw new HttpError(StatusCodes.CONFLICT, 'Exam code already exists');
        }

        const teacher: User = await User.findByPk(userId);
        const newExam = await Examination.create({
            examCode: examPayload.examCode,
            detail: examPayload.details,
            openAt: examPayload.openAt,
            closeAt: examPayload.closeAt,
            creator: teacher
        });

        return ExamDTO.toDTO(newExam);
    }

    async editExam(examPayload: ExamPayload, examId: number) {
        const exam = await Examination.findByPk(examId, {
            include: [{
                model: User,
                as: 'creator'
            }]
        });
        if (!exam) {
            throw new NotFoundError('Exam not found');
        }

        // check if exam code is already exists
        const existingExam = await Examination.findOne({
            where: {
                examCode: examPayload.examCode
            }
        });
        if (existingExam && existingExam.id !== examId) {
            throw new HttpError(StatusCodes.CONFLICT, 'Tên ca thi đã tồn tại!');
        }

        let submittedRecord = await exam.$get("record");
        if (examPayload.openAt != exam.openAt && submittedRecord.length > 0) {
            throw new BadRequestError("Không thể thay đổi thời gian bắt đầu khi đã có người điểm danh!");
        }

        // update exam
        exam.examCode = examPayload.examCode;
        exam.detail = examPayload.details;
        exam.openAt = examPayload.openAt;
        exam.closeAt = examPayload.closeAt;

        await exam.save();

        return ExamDTO.toDTO(exam);
    }

    async isAbleToTakeExam(examCode: string) {
        const exam: Examination = await Examination.findOne({
            where: {
                examCode: examCode
            }
        });
        if (!exam) {
            throw new NotFoundError('Exam not found');
        }

        const now = new Date();
        if (now.getTime() < exam.openAt.getTime() || now.getTime() > exam.closeAt.getTime()) {
            throw new HttpError(StatusCodes.FORBIDDEN, 'Not now');
        }

    }
}