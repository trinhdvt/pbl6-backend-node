import {Service} from "typedi";
import RecordPayload from "../dto/request/RecordPayload";
import AttendancePayload from "../dto/request/AttendancePayload";
import Examination from "../models/Examination";
import AttendanceRecord from "../models/AttendanceRecord";
import {NotFoundError} from "routing-controllers";
import RecordDTO from "../dto/response/RecordDTO";

@Service()
export default class ExamRecordService {

    async getAttendance(examId: number) {
        const exam = await Examination.findByPk(examId, {
            include: [{
                model: AttendanceRecord,
                as: 'record'
            }]
        });
        if (!exam) {
            throw new NotFoundError('Examination not found');
        }

        return exam.record.map(record => RecordDTO.toDTO(record));
    }

    async modifyAttendance(recordId: number, recordPayload: RecordPayload) {
        let record = await AttendanceRecord.findByPk(recordId);
        if (!record) {
            throw new NotFoundError('Record not found');
        }

        record.status = recordPayload.status;
        record.studentId = recordPayload.studentId;
        record.studentName = recordPayload.studentName;
        record.birthday = recordPayload.birthday;
        record.facultyName = recordPayload.facultyName;
        record.className = recordPayload.className;
        record.year = recordPayload.year;

        return RecordDTO.toDTO(await record.save());
    }

    async submitRecord(payload: AttendancePayload) {
        const {examCode} = payload;
        const exam = await Examination.findOne({
            where: {
                examCode: examCode
            }
        });
        if (!exam) {
            throw new NotFoundError('Examination not found');
        }

        const newRecord = await AttendanceRecord.create({
            examId: exam.id,
            checkAt: payload.checkAt ?? new Date(),
            status: payload.status,
            studentId: payload.studentId,
            studentName: payload.studentName,
            birthday: payload.birthday,
            facultyName: payload.facultyName,
            className: payload.className,
            year: payload.year,
            faceImg: payload.faceImg,
            cardImg: payload.cardImg,
            cropCard: payload.cropCard,
            ipAddress: payload.ipAddress,
            userAgent: payload.userAgent
        });

        return RecordDTO.toDTO(newRecord);
    }
}
