import {Service} from "typedi";
import JwtService from "./JwtService";
import bcrypt from "bcrypt";
import {NotFoundError, UnauthorizedError} from "routing-controllers";
import User from "../models/User";


@Service()
export default class AuthService {

    constructor(private jwtService: JwtService) {

    }

    public login = async (email: string, password: string) => {
        const acc = await User.findOne({
            where: {
                email: email
            }
        });

        if (!acc) {
            throw new NotFoundError(`Invalid Email or Password`)
        }

        const isPasswordCorrect = await this.checkPassword(password, acc.password);
        if (!isPasswordCorrect) {
            throw new UnauthorizedError("Invalid ID or Password");
        }

        const {accessToken, refreshToken} = await this.jwtService.createToken(acc);
        return {accessToken, refreshToken};
    }


    public reCreateToken = async (accessToken: string, refreshToken: string) => {
        let isValid = await this.jwtService.isValidToRefreshToken(accessToken, refreshToken);
        if (!isValid) {
            throw new UnauthorizedError("Not eligible to refresh token");
        }

        const {id} = this.jwtService.getPayLoad(accessToken);
        const acc = await User.findByPk(id);
        if (!acc) {
            throw new UnauthorizedError("Not exist");
        }

        return await this.jwtService.createToken(acc);
    }


    public encryptPassword = async (password: string) => {
        return await bcrypt.hash(password, 10);
    }

    public checkPassword = async (rawPassword: string, encryptedPassword: string) => {
        return await bcrypt.compare(rawPassword, encryptedPassword);
    }


}