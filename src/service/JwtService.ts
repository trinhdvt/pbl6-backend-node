import {Service} from "typedi";
import jwt, {JwtPayload, TokenExpiredError} from "jsonwebtoken";
import {ForbiddenError} from "routing-controllers";
import User from "../models/User";

@Service()
export default class JwtService {
    private readonly SECRET_KEY: string;
    private readonly EXPIRE_TIME: string;

    constructor() {
        this.SECRET_KEY = Buffer.from(process.env.JWT_SECRET ?? "godsaveus").toString("base64");
        this.EXPIRE_TIME = process.env.JWT_TIME ?? "10000000";
    }

    public createToken = async (user: User) => {
        const payload = {
            id: user.id,
            email: user.email,
            role: user.role
        }

        const accessToken = jwt.sign(payload,
            this.SECRET_KEY,
            {
                expiresIn: this.EXPIRE_TIME
            });

        // const duration = ms(this.EXPIRE_TIME);

        const refreshToken = await this.createRefreshToken(accessToken, user);
        return {accessToken, refreshToken};
    }

    public isValidToRefreshToken = async (accessToken: string, refreshToken: string) => {
        const {id} = this.getPayLoad(accessToken);
        const user: User = await User.findByPk(id);

        return (user != null
            && user.refreshToken === refreshToken
            && user.accessToken === accessToken);
    }

    public isValidJwt = (token: string, ignoreExpiration: boolean = false) => {
        try {
            jwt.verify(token, this.SECRET_KEY, {ignoreExpiration: ignoreExpiration});
            return true;
        } catch (err) {
            if (err instanceof TokenExpiredError && !ignoreExpiration) {
                return false;
            }
            // invalid token
            throw new ForbiddenError("Invalid token");
        }
    };

    public getPayLoad = (accessToken: string) => {
        try {
            const decoded = jwt.verify(accessToken, this.SECRET_KEY, {ignoreExpiration: true}) as JwtPayload;

            return {
                id: decoded.id,
                email: decoded.email,
                role: decoded.role
            };
        } catch (e) {
            // invalid token
            throw new ForbiddenError("Invalid token");
        }
    }

    private createRefreshToken = async (accessToken: string, user: User) => {
        const randomString = Math.random().toString(36).substring(2);
        const rfToken = Buffer.from(randomString).toString("base64");

        // save token to db
        user.accessToken = accessToken;
        user.refreshToken = rfToken;
        await user.save();

        //
        return rfToken;
    }

}
