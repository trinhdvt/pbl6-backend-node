import express from "express";
import sequelize from "./models";
import cookieParser from "cookie-parser";
import bodyParser from "body-parser";
import cors from "cors";
import morgan from "morgan";
import compression from "compression";

export default class App {

    private readonly app: express.Application;

    constructor() {
        this.app = express();
    }

    private static async initDatabase() {
        await sequelize.sync();
    }

    public listen() {
        const port: number = parseInt(process.env.PORT ?? '8080');

        App.initDatabase().then(() => {
            console.log('Database initialized!')

            this.app.listen(port, () => {
                console.log(`Server listening on port ${port}`);
            });

        }).catch(err => console.error(`Error when initial database${err}`));

    }

    public bootstrap() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));
        this.app.use(cookieParser());
        this.app.use(cors());
        this.app.use(compression());
        this.app.use(morgan(':req[X-Real-IP] :req[X-Forwarded-Host] [:date[clf]] :method :url :status - :response-time ms'));
    }

    public getServer(): express.Application {
        return this.app;
    }
}
