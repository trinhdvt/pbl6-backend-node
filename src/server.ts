require('dotenv').config();
import "reflect-metadata"
import App from "./app";
import {useContainer, useExpressServer} from "routing-controllers";
import {Container} from "typedi";
import {CurrentUserChecker, PreAuthorize} from "./middlewares/JwtFilterMiddleware";

useContainer(Container);

const app = new App();
// create an express Application with common middleware
app.bootstrap();

// inject controller
useExpressServer(app.getServer(), {
    development: false,
    defaults: {
        paramOptions: {
            required: true
        }
    },
    routePrefix: "/api",
    controllers: [__dirname + "/controller/*.js"],
    authorizationChecker: PreAuthorize,
    currentUserChecker: CurrentUserChecker
})

process.env.TZ = "Asia/Ho_Chi_Minh"

// connect to database then start server (default port is 8080)
app.listen();
