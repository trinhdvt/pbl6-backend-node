FROM node:alpine3.14 as builder

WORKDIR /app

COPY package.json .
COPY tsconfig.json .

RUN npm install

COPY src src

RUN npm run build

## Copy the built files to the output directory

FROM node:alpine3.14

WORKDIR /app

ENV JWT_SECRET=94gOYLLzIaDjFBZVNgsXRNVjj569TwCa36s6F2dGBnYdwreLBWyoK28UTy1MdAB
ENV JWT_TIME=10000000

COPY --from=builder /app/package.json .

RUN npm install --only=production && npm install pm2 -g

COPY --from=builder /app/target target

CMD ["pm2-runtime","./target/server.js"]


